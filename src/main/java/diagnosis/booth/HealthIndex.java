package diagnosis.booth;

import diagnosis.Factorable;

/**
 * The health index is used to determine the medical unit. It implements {@link Factorable} because determining the medical units depends on the health index factor.
 */
public class HealthIndex implements Factorable {
    private final int value;

    /**
     * Creates a new instance of {@link HealthIndex}.
     *
     * @param value the {@link HealthIndex} value.
     */
    public HealthIndex(final int value) {
        this.value = value;
    }

    @Override
    public final boolean isFactorableBy(final int factor) {
        final int dividedNumber = this.value / factor;
        return (dividedNumber * factor) == this.value;
    }
}
