package diagnosis.booth;

/**
 * The medical unit where a patient needs to go.
 */
public enum MedicalUnit {
    CARDIOLOGY("cardiology center"),
    TRAUMA("trauma center");

    private final String name;

    MedicalUnit(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name;
    }
}

