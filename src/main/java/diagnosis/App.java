package diagnosis;

import diagnosis.booth.MedicalUnit;
import diagnosis.input.ConsoleSensor;
import diagnosis.booth.SelfDiagnosis;
import diagnosis.output.ConsoleWriter;

/**
 * The coding test main application.
 */
public class App {
    /**
     * Starts the main application.
     * <p>
     * Instantiates a new {@link SelfDiagnosis} with a mocked sensor and a mocked writer. Then diagnoses the disease using the sensor data and displays the {@link MedicalUnit} where the patient needs to be redirected using the writer.
     *
     * @param args unused.
     */
    public static void main(final String... args) {
        final SelfDiagnosis diagnosis = new SelfDiagnosis(new ConsoleSensor(), new ConsoleWriter());
        diagnosis.diagnose();
    }
}
