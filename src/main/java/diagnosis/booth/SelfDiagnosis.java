package diagnosis.booth;

import diagnosis.Factorable;
import diagnosis.Sensor;
import diagnosis.Writer;

import static diagnosis.booth.MedicalUnit.*;

/**
 * The self diagnosis. Can be used to {@link SelfDiagnosis#diagnose() do a diagnostic}.
 */
public class SelfDiagnosis {
    private final Sensor sensor;
    private final Writer writer;

    /**
     * Creates a new {@link SelfDiagnosis}.
     *
     * @param sensor the sensor used to get numerical data to know what to write with the writer.
     * @param writer the writer used to write the diagnosis result.
     */
    public SelfDiagnosis(final Sensor sensor, final Writer writer) {
        this.sensor = sensor;
        this.writer = writer;
    }

    /**
     * Diagnoses the disease of the user of this {@link SelfDiagnosis}.
     * <p>
     * Uses the sensor to get numerical data, then deduce a {@link HealthIndex}.
     * Depending on this health index, uses the writer to indicate the {@link MedicalUnit} where the user needs to go.
     */
    public final void diagnose() {
        final int measurement = this.sensor.measure();
        final Factorable healthIndex = new HealthIndex(measurement);
        if (healthIndex.isFactorableBy(3)) {
            final String cardiology = CARDIOLOGY.toString();
            writer.write(cardiology);
        }
        if (healthIndex.isFactorableBy(5)) {
            final String trauma = TRAUMA.toString();
            writer.write(trauma);
        }
    }
}
