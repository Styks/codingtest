package diagnosis;

import diagnosis.booth.SelfDiagnosis;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static diagnosis.booth.MedicalUnit.*;
import static org.mockito.Mockito.*;

class SelfDiagnosisTest {
    @ParameterizedTest
    @ValueSource(ints = {3, 6, 33, 9639})
    void diagnose_multipleOf3_callWriterWithCardiology(final int measurement) {
        // Arrange
        final Sensor sensor = mock(Sensor.class);
        when(sensor.measure()).thenReturn(measurement);
        final Writer writer = mock(Writer.class);
        final SelfDiagnosis diagnosis = new SelfDiagnosis(sensor, writer);

        // Act
        diagnosis.diagnose();

        // Assert
        final String expectedMedicalUnit = CARDIOLOGY.toString();
        verify(writer, times(1)).write(expectedMedicalUnit);
    }

    @ParameterizedTest
    @ValueSource(ints = {5, 10, 55, 9635})
    void diagnose_multipleOf5_callWriterWithTrauma(final int measurement) {
        // Arrange
        final Sensor sensor = mock(Sensor.class);
        when(sensor.measure()).thenReturn(measurement);
        final Writer writer = mock(Writer.class);
        final SelfDiagnosis diagnosis = new SelfDiagnosis(sensor, writer);

        // Act
        diagnosis.diagnose();

        // Assert
        final String expectedMedicalUnit = TRAUMA.toString();
        verify(writer, times(1)).write(expectedMedicalUnit);
    }

    @ParameterizedTest
    @ValueSource(ints = {3*5, 6*10, 33*55, 9639*9635})
    void diagnose_multipleOf3And5_callWriterWithCardiologyAndTrauma(final int measurement) {
        // Arrange
        final Sensor sensor = mock(Sensor.class);
        when(sensor.measure()).thenReturn(measurement);
        final Writer writer = mock(Writer.class);
        final SelfDiagnosis diagnosis = new SelfDiagnosis(sensor, writer);

        // Act
        diagnosis.diagnose();

        // Assert
        final String expectedMedicalUnit1 = CARDIOLOGY.toString();
        final String expectedMedicalUnit2 = TRAUMA.toString();
        verify(writer, times(1)).write(expectedMedicalUnit1);
        verify(writer, times(1)).write(expectedMedicalUnit2);
    }

    @Test
    void diagnose_neither3Nor5Multiple_doNotCallWriter() {
        // Arrange
        final Sensor sensor = mock(Sensor.class);
        when(sensor.measure()).thenReturn(2);
        final Writer writer = mock(Writer.class);
        final SelfDiagnosis diagnosis = new SelfDiagnosis(sensor, writer);

        // Act
        diagnosis.diagnose();

        // Assert
        verifyNoInteractions(writer);
    }
}
