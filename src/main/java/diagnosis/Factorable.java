package diagnosis;

/**
 * A factorable object able to return whether it is factorable or not.
 */
@FunctionalInterface
public interface Factorable {
    /**
     * Whether this object is factorable by the given factor.
     *
     * @param factor the potential factor of this object.
     * @return true, if this object is factorable by the given factor, false otherwise.
     */
    boolean isFactorableBy(int factor);
}
