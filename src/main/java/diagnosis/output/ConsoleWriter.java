package diagnosis.output;

import diagnosis.Writer;

/**
 * The mocked implementation of a writer. Uses the console output to display the diagnosis result.
 */
public class ConsoleWriter implements Writer {
    @Override
    public final void write(final String text) {
        System.out.println(text);
    }
}
