package diagnosis;

/**
 * A sensor able to return a measurement.
 */
@FunctionalInterface
public interface Sensor {
    /**
     * Measures and returns the measurement.
     *
     * @return the measurement.
     */
    int measure();
}
