package diagnosis.input;

import diagnosis.Sensor;

import java.util.Scanner;

/**
 * The mocked implementation of a sensor. Uses the console input as numerical data.
 */
public class ConsoleSensor implements Sensor {
    @Override
    public final int measure() {
        try (final Scanner scanner = new Scanner(System.in)) {
            System.out.println("Enter an index:");
            final String measure = scanner.nextLine();
            return Integer.parseInt(measure);
        }
    }
}
