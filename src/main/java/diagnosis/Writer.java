package diagnosis;

/**
 * A writer able to write.
 */
@FunctionalInterface
public interface Writer {
    /**
     * Writes a text.
     *
     * @param text the text to write.
     */
    void write(String text);
}
